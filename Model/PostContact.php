<?php

class PostContact
{
    protected $curl;

    public function __construct()
    {
        $this->curl = new Curl();
    }

    public function postContact($link, $custom_fields, $post)
    {
        $data = $this->prepare($custom_fields, $post);

        $this->curl->setOptionsContact($link, $data);
        $result = $this->curl->query();

        return $result;
    }

    private function prepare($custom_fields, $post)
    {
        $data= [
            'name'=>isset($post['name']) ? $post['name'] : 'ss',
            'company'=>isset($post['company']) ? $post['company'] : '',
            'position'=>isset($post['position']) ? $post['position'] : '',
            'phone'=>isset($post['phone']) ? $post['phone'] : '',
            'email'=>isset($post['email']) ? $post['email'] : 'sd',
            'web'=>isset($post['web']) ? $post['web'] : '',
            'jabber'=>isset($post['jabber']) ? $post['jabber'] : '',
            'scope'=>isset($post['scope']) && is_array($post['scope']) ? $post['scope'] : array()
        ];

        $contact=[
            'name'=>$data['name'],
            'custom_fields'=>array(
                array(
                    'id'=>$custom_fields['EMAIL'],
                    'values'=>array(
                        array(
                            'value'=>$data['email'],
                            'enum'=>'WORK'
                        )
                    )
                )
            )
        ];

        if(!empty($data['company']))
            $contact+=array('company_name'=>$data['company']);
        if(!empty($data['position']))
            $contact['custom_fields'][]=array(
                'id'=>$custom_fields['POSITION'],
                'values'=>array(
                    array(
                        'value'=>$data['position']
                    )
                )
            );
        if(!empty($data['phone']))
            $contact['custom_fields'][]=array(
                'id'=>$custom_fields['PHONE'],
                'values'=>array(
                    array(
                        'value'=>$data['phone'],
                        'enum'=>'OTHER'
                    )
                )
            );

        $set['request']['contacts']['add'][]=$contact;

        return $set;
    }
}