<?php


class Curl
{
    private $curl;
    private $data;
    private $method;

    public function __construct()
    {
        $this->curl = curl_init();
    }

    public function setOptionsAuth($link, $data, $method)
    {
        $this->data = $data;
        $this->method = $method;

        curl_setopt($this->curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($this->curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
        curl_setopt($this->curl,CURLOPT_URL,$link);
        curl_setopt($this->curl,CURLOPT_POST,true);
        curl_setopt($this->curl,CURLOPT_POSTFIELDS,http_build_query($this->data));
        curl_setopt($this->curl,CURLOPT_HEADER,false);
        curl_setopt($this->curl,CURLOPT_COOKIEFILE,dirname(__DIR__).'/cookie.txt');
        curl_setopt($this->curl,CURLOPT_COOKIEJAR,dirname(__DIR__).'/cookie.txt');
        curl_setopt($this->curl,CURLOPT_SSL_VERIFYPEER,0);

    }

    public function setOptionsAccount($link)
    {
        curl_setopt($this->curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($this->curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
        curl_setopt($this->curl,CURLOPT_URL,$link);
        curl_setopt($this->curl,CURLOPT_HEADER,false);
        curl_setopt($this->curl,CURLOPT_COOKIEFILE,dirname(__DIR__).'/cookie.txt');
        curl_setopt($this->curl,CURLOPT_COOKIEJAR,dirname(__DIR__).'/cookie.txt');
        curl_setopt($this->curl,CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($this->curl,CURLOPT_SSL_VERIFYHOST,0);
    }

    public function setOptionsContact($link, $data)
    {
        curl_setopt($this->curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($this->curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
        curl_setopt($this->curl,CURLOPT_URL,$link);
        curl_setopt($this->curl,CURLOPT_CUSTOMREQUEST,'POST');
        curl_setopt($this->curl,CURLOPT_POSTFIELDS,json_encode($data));
        curl_setopt($this->curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
        curl_setopt($this->curl,CURLOPT_HEADER,false);
        curl_setopt($this->curl,CURLOPT_COOKIEFILE,dirname(__DIR__).'/cookie.txt');
        curl_setopt($this->curl,CURLOPT_COOKIEJAR,dirname(__DIR__).'/cookie.txt');
        curl_setopt($this->curl,CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($this->curl,CURLOPT_SSL_VERIFYHOST,0);
    }

    public function query()
    {
        $out=curl_exec($this->curl);
        $code=curl_getinfo($this->curl,CURLINFO_HTTP_CODE);

        $this->CheckCurlResponse($code);

        $response = ['response' => $out, 'status' => $code];
        return $response;
    }

    public function __destruct()
    {
       curl_close($this->curl);
    }

    public function CheckCurlResponse($code)
    {
        $code=(int)$code;
        $errors=array(
            301=>'Moved permanently',
            400=>'Bad request',
            401=>'Unauthorized',
            403=>'Forbidden',
            404=>'Not found',
            500=>'Internal server error',
            502=>'Bad gateway',
            503=>'Service unavailable'
        );
        try
        {
            #Если код ответа не равен 200 или 204 - возвращаем сообщение об ошибке
            if($code!=200 && $code!=204)
                throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error',$code);
        }
        catch(Exception $E)
        {
            die('Ошибка: '.$E->getMessage().PHP_EOL.'Код ошибки: '.$E->getCode());
        }
    }
}