<?php

class Account
{
    private $curl;

    public function __construct()
    {
        $this->curl = new Curl();
    }

    public function getAccount($link)
    {
        $this->curl->setOptionsAccount($link);
        $result = $this->curl->query();

        $Response=json_decode($result['response'],true);
        $account=$Response['response']['account'];

        $result = $this->prepare($account);

        return $result;
    }

    private function prepare($account)
    {
        $need=array_flip(array('POSITION','PHONE','EMAIL'));
        if(isset($account['custom_fields'],$account['custom_fields']['contacts']))
            do
            {
                foreach($account['custom_fields']['contacts'] as $field)
                    if(is_array($field) && isset($field['id']))
                    {
                        if(isset($field['code']) && isset($need[$field['code']]))
                            $fields[$field['code']]=(int)$field['id'];

                        elseif(isset($field['name']) && $field['name']=='Сфера деятельности')
                            $fields['SCOPE']=$field;

                        $diff=array_diff_key($need,$fields);
                        if(empty($diff))
                            break 2;
                    }
                if(isset($diff))
                    die('В amoCRM отсутствуют следующие поля'.': '.join(', ',$diff));
                else
                    die('Невозможно получить дополнительные поля');
            }
            while(false);
        else
            die('Невозможно получить дополнительные поля');
        $custom_fields=isset($fields) ? $fields : false;

        return $custom_fields;
    }
}